This is a plugin designed by the Avalanche Coding Team, for the specific purpose of having fun on Minecraft. Contribution to this plugin is easy. Either submit a pull request with an idea you have and would like to see implemented, or send the team a message asking to be added to the development team. I will ask a series of questions to determine your coding level, and if I find it sufficient I will accept you to the team.

The plugin is designed by the community, so don't be afraid to ask! If it's impossible of stupidly difficult, I or other developers will probably not implement your idea. If it's reasonable and we think we can do it, we'll give it a try!

This plugin was designed for the NoNamedOrg server but is now asynchronous for multi-server usage.

This is a Minecraft Bukkit/Spigot plugin, not a standalone Java program.

This work is copyrighted by the Avalanche Coding Team, with accreditation proceeding to Camzie99 (Cameron Redmore) for his wonderful Command System, and to Steven Lawson (Madgeek1450) and Jerom Sar (Prozza) for a few commands from their TotalFreedomMod plugin. Thanks to Bukkit and Minecraft for making a Java environment that is easily manipulated, and to Spigot for making the ravenous cavern between mods and plugins that much more smaller.

Any questions, feel free to send me a message.